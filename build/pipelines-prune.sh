#!/bin/bash

# Get non-running pipelines from jobs API resource:
PIPELINES=$(curl --silent --header "PRIVATE-TOKEN: ${BUILDER_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/jobs" | jq -r '[.[].pipeline | select(.status != "running") | .id] | unique | .[]')

# Delete pipelines
for PIPELINE in ${PIPELINES}
do
  curl --silent --header "PRIVATE-TOKEN: ${BUILDER_TOKEN}" --request "DELETE" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${PIPELINE}"
done

# Get remaining non-running pipelines from pipelines API resource:
PIPELINES=$(curl --silent --header "PRIVATE-TOKEN: ${BUILDER_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines" | jq -r ".[] | select(.status != \"running\") | .id")

# Delete pipelines
for PIPELINE in ${PIPELINES}
do
  curl --silent --header "PRIVATE-TOKEN: ${BUILDER_TOKEN}" --request "DELETE" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${PIPELINE}"
done