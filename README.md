[![pipeline status](https://gitlab.com/grunlab/builder/badges/main/pipeline.svg)](https://gitlab.com/grunlab/builder/-/commits/main)

# GrunLab Builder

Builder container image.

Docs: https://docs.grunlab.net/images/builder.md

Base image: [grunlab/base-image/debian:12][base-image]

Format: docker

Supported architecture(s):
- arm64

GrunLab project(s) using this image:
- [grunlab/gitlab-runner][gitlab-runner]

[base-image]: <https://gitlab.com/grunlab/base-image>
[gitlab-runner]: <https://gitlab.com/grunlab/gitlab-runner>